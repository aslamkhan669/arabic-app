import {ErrorHandler, NgModule} from "@angular/core";
import {IonicApp, IonicModule, IonicErrorHandler,NavController, Nav} from "ionic-angular";
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { HttpModule } from '@angular/http';
import {IonicStorageModule} from '@ionic/storage';
import {AuthenticationService} from "../Services/authenticate-service";
import {HomeService} from "../services/home-service";
import {ImgCacheModule} from 'ng-imgcache';
import { NativeStorage } from '@ionic-native/native-storage';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';
import { FileTransfer } from '@ionic-native/file-transfer';
import { ImagesProvider } from '../providers/images/images';
import { AudioProvider } from '../providers/audio/audio';
import { Camera } from '@ionic-native/camera';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Media } from '@ionic-native/media';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import {MyApp} from "./app.component";
import {CameraPage} from "../pages/camera/camera";
import {MicrophonePage} from "../pages/microphone/microphone";
import {HomePage} from "../pages/home/home";
import {LoginPage} from "../pages/login/login";
import {RegisterPage} from "../pages/register/register";
import {BookDetailPage} from "../pages/book-detail/book-detail";
import {BooksPage} from "../pages/books/books";
import {DuesPage} from "../pages/dues/dues";
import {NewAssignmentPage} from "../pages/newassignment/newassignment";
import { UploadModalPage } from '../pages/upload-modal/upload-modal';
import { UploadpicsPage } from '../pages/uploadpics/uploadpics';
import { PreviewModalPage } from '../pages/preview-modal/preview-modal';
import { NewAssignment_DetailsPage } from '../pages/newassignment_details/newassignment_details';
import { errorHandler } from "@angular/platform-browser/src/browser";


@NgModule({
  declarations: [
    MyApp,
    CameraPage,
    MicrophonePage,
    HomePage,
    LoginPage,
    RegisterPage,
    BookDetailPage,
    BooksPage,
    DuesPage,
    UploadModalPage,
    PreviewModalPage,
    NewAssignmentPage,
    UploadpicsPage,
    NewAssignment_DetailsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    ImgCacheModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    IonicStorageModule.forRoot({
      name: 'Arabic School',
        driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CameraPage,
    MicrophonePage,
    HomePage,
    LoginPage,
    RegisterPage,
    BookDetailPage,
    DuesPage,
    UploadModalPage,
    PreviewModalPage,
    NewAssignmentPage,
    UploadpicsPage,
    NewAssignment_DetailsPage,
    BooksPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    {provide:ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    InAppBrowser,
    File,
    FilePath,
    Media,
    AuthenticationService,
    HomeService,
    ImagesProvider,
    AudioProvider,
    FileTransfer,
    NativeStorage,
    PhotoViewer,
    NewAssignmentPage,
    BooksPage,
    
    
  ]
})

export class AppModule {
}
