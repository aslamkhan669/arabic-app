var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { Http, Headers } from '@angular/http';
import * as Config from '../config';
var AuthenticationService = (function () {
    function AuthenticationService(nativeStorage, http) {
        this.nativeStorage = nativeStorage;
        this.http = http;
    }
    AuthenticationService.prototype.getUser = function () {
        return this.nativeStorage.getItem('User');
    };
    AuthenticationService.prototype.setUser = function (user) {
        return this.nativeStorage.setItem('User', user);
    };
    AuthenticationService.prototype.logOut = function () {
        return this.nativeStorage.clear();
    };
    AuthenticationService.prototype.doLogin = function (username, password) {
        return this.http.post(Config.WORDPRESS_URL + 'wp-json/jwt-auth/v1/token', function (data) {
            return {
                username: data.user_nicename,
                token: data.token
            };
        });
    };
    // doRegister(user_data, token){
    //   return this.http.post(Config.WORDPRESS_REST_API_URL + 'users?token=' + token, user_data);
    // }
    AuthenticationService.prototype.validateAuthToken = function (token) {
        var header = new Headers();
        header.append('Authorization', 'Basic ' + token);
        return this.http.post(Config.WORDPRESS_URL + 'wp-json/jwt-auth/v1/token/validate?token=' + token, {}, { headers: header });
    };
    return AuthenticationService;
}());
AuthenticationService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [NativeStorage,
        Http])
], AuthenticationService);
export { AuthenticationService };
// import { Injectable } from '@angular/core';
// import { Http, Headers, Response, RequestOptions } from '@angular/http';
// import { Observable } from 'rxjs/Observable';
// import { Subject } from 'rxjs/Subject';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/observable/of';
// import { environment } from '../environment/environment';
// const constURL = `${environment.constURL}`;
// @Injectable()
// export class AuthenticationService {
//   // Observable string sources
//   private emitChangeSource = new Subject<any>();
//   // Observable string streams
//   changeEmitted$ = this.emitChangeSource.asObservable();
//   constructor( private http: Http ) { }
//   login(username: string, password: string): Observable<boolean> {
//     return this.http.post(`${constURL}/login/authenticate`, { username, password })
//       .map(res => {
//         var data =res;
//         console.log(data);
//         return true;
//       })
//       .catch(err => {
//         console.log(err);
//         return Observable.of(false);
//       });
//   }
//   logout(): Observable<boolean> {
//     // clear token remove user from local storage to log user out
//     return this.http.delete(`${constURL}/users/`)
//       .map(res => {
//         return true;
//       })
//       .catch(err => {
//         return Observable.of(false);
//       });
//   }
//   getUser() {
//     return this.http.get(`${constURL}/users/status`)
//       .map(res => res.json());
//   }
//   emitChange(change) {
//     this.emitChangeSource.next(change);
//   }
// }
//# sourceMappingURL=authentication.service.js.map