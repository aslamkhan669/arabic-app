var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from "@angular/core";
import { Platform, Nav } from "ionic-angular";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';
import { AuthenticationService } from "../Services/authenticate-service";
import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { ImgCacheService } from 'ng-imgcache';
import { Storage } from '@ionic/storage';
var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, keyboard, imgCache, storage, user) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.keyboard = keyboard;
        this.imgCache = imgCache;
        this.storage = storage;
        this.user = user;
        this.rootPage = LoginPage;
        this.initializeApp();
        this.appMenuItems = [
            { title: 'Home', component: HomePage, icon: 'home' },
            { title: 'Log Out', component: LoginPage, icon: 'log-out' }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            //*** Control Splash Screen
            // this.splashScreen.show();
            // this.splashScreen.hide();
            //*** Control Status Bar
            _this.statusBar.styleDefault();
            _this.statusBar.overlaysWebView(false);
            //*** Control Keyboard
            _this.keyboard.disableScroll(true);
        });
    };
    MyApp.prototype.openPage = function (page) {
        var _this = this;
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        debugger;
        if (page.title == "Log Out") {
            var data = { verified: false };
            this.user.setUser(data).then(function (u) {
                _this.nav.setRoot(LoginPage);
            }).catch(function (e) { });
        }
        else {
            this.nav.push(page.component);
        }
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.logout = function () {
        debugger;
        this.nav.setRoot(LoginPage);
    };
    return MyApp;
}());
__decorate([
    ViewChild(Nav),
    __metadata("design:type", Nav)
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Component({
        templateUrl: 'app.html'
    }),
    __metadata("design:paramtypes", [Platform,
        StatusBar,
        SplashScreen,
        Keyboard,
        ImgCacheService,
        Storage,
        AuthenticationService])
], MyApp);
export { MyApp };
//# sourceMappingURL=app.component.js.map