import { Component, ViewChild } from "@angular/core";
import { Platform, Nav } from "ionic-angular";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';
import {AuthenticationService} from "../Services/authenticate-service";
import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import {ImgCacheService} from 'ng-imgcache'
import {Storage} from '@ionic/storage';


export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  appMenuItems: Array<MenuItem>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public keyboard: Keyboard,
    public imgCache : ImgCacheService,
    public storage : Storage,
    public user : AuthenticationService,
    
  ) {
    this.initializeApp();

    this.appMenuItems = [
      {title: 'Home', component: HomePage, icon: 'home'},
      {title: 'Log Out', component: LoginPage, icon: 'log-out'}
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {

      
      // Okay, so the platform is ready and our plugins are available.

      //*** Control Splash Screen
      // this.splashScreen.show();
      // this.splashScreen.hide();

      //*** Control Status Bar
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);

      //*** Control Keyboard
      this.keyboard.disableScroll(true);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.title == "Log Out"){
      var data = {verified:false};
      this.user.setUser(data).then(u=>{       
        this.nav.setRoot(LoginPage);
      }).catch(e=>{});
    }else{
      this.nav.push(page.component);
    }
    //this.nav.setRoot(page.component);
  }

  logout() {
    this.nav.setRoot(LoginPage);
  }
}
