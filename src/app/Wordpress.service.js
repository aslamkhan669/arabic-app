var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import * as Config from '../config';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';
var WordpressService = (function () {
    function WordpressService(http) {
        this.http = http;
    }
    WordpressService.prototype.getRecentPosts = function (categoryId, page) {
        if (page === void 0) { page = 1; }
        //if we want to query posts by category
        var category_url = categoryId ? ("&categories=" + categoryId) : "";
        return this.http.get(Config.WORDPRESS_REST_API_URL
            + 'posts?page=' + page
            + category_url)
            .map(function (res) { return res.json(); });
    };
    WordpressService.prototype.getComments = function (postId, page) {
        if (page === void 0) { page = 1; }
        return this.http.get(Config.WORDPRESS_REST_API_URL
            + "comments?post=" + postId
            + '&page=' + page)
            .map(function (res) { return res.json(); });
    };
    WordpressService.prototype.getAuthor = function (author) {
        return this.http.get(Config.WORDPRESS_REST_API_URL + "users/" + author)
            .map(function (res) { return res.json(); });
    };
    WordpressService.prototype.getPostCategories = function (post) {
        var _this = this;
        var observableBatch = [];
        post.categories.forEach(function (category) {
            observableBatch.push(_this.getCategory(category));
        });
        return Observable.forkJoin(observableBatch);
    };
    WordpressService.prototype.getCategory = function (category) {
        return this.http.get(Config.WORDPRESS_REST_API_URL + "categories/" + category)
            .map(function (res) { return res.json(); });
    };
    WordpressService.prototype.createComment = function (postId, user, comment) {
        var header = new Headers();
        header.append('Authorization', 'Bearer ' + user.token);
        return this.http.post(Config.WORDPRESS_REST_API_URL + "comments?token=" + user.token, {
            author_name: user.displayname,
            author_email: user.email,
            post: postId,
            content: comment
        }, { headers: header })
            .map(function (res) { return res.json(); });
    };
    return WordpressService;
}());
WordpressService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http])
], WordpressService);
export { WordpressService };
//# sourceMappingURL=wordpress.service.js.map