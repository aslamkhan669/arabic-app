var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Environment } from "../environment/environment";
import { Storage } from '@ionic/storage';
// const httpOptions =  {
//   headers: new Headers({ 'Content-Type':'application/json','Authorization':'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXJhYmljLWNsYXNzcm9vbS5jb20iLCJpYXQiOjE1Mjc3NjA3OTQsIm5iZiI6MTUyNzc2MDc5NCwiZXhwIjoxNTI4MzY1NTk0LCJkYXRhIjp7InVzZXIiOnsiaWQiOiIxMSJ9fX0.KT8ovxx6pDcKTbg1uLh51UGJCbZVNpRUN74s8i6XaFw' })
// }; 
var HomeService = (function () {
    function HomeService(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    HomeService.prototype.getallAssignment = function (token) {
        var httpOptions = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token }) };
        return this.http.get("https://arabic-classroom.com/wp-json/wp/v2/assignments", httpOptions);
    };
    HomeService.prototype.getNewAssignments = function (token) {
        var httpOptions = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token }) };
        return this.http.get("https://arabic-classroom.com/wp-json/wp/v2/new_assignments", httpOptions);
    };
    HomeService.prototype.deleteAssignment = function (token, assignId, courseId, lessonId) {
        var httpOptions = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token }) };
        return this.http.delete("https://arabic-classroom.com/wp-json/wp/v2/assignments/" + assignId + '?lesson_id=' + lessonId + '&course_id=' + courseId + '&force=' + 1, httpOptions);
    };
    HomeService.prototype.getallAssignmentImages = function (id) {
        var payload = { assignid: id };
        return this.http.post(Environment.URL + "api/product/imagesbycategories", payload, this.httpOptions);
    };
    HomeService.prototype.getAssignmentById = function (token) {
        var httpOptions = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token }) };
        return this.http.get("https://arabic-classroom.com/wp-json/wp/v2/assignments", httpOptions);
    };
    return HomeService;
}());
HomeService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http, Storage])
], HomeService);
export { HomeService };
//# sourceMappingURL=home-service.js.map