import {Injectable} from "@angular/core";
import {Observable} from "rxjs/observable";
import {Http,Headers,Response,RequestOptions} from "@angular/http";
import {Environment} from "../environment/environment";
import { Storage } from '@ionic/storage';
 
const STORAGE_KEY = 'user';

const httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
};

@Injectable()

export class AuthenticationService {
  private http:any;
  
  constructor(http: Http,public storage: Storage){
    this.http = http;
  }

  signin(email:string,password:string) {
    var body = {username:email,password:password};      
    return this.http.post("https://arabic-classroom.com/wp-json/jwt-auth/v1/token",body);  
  }

  setUser(user){
       return this.storage.set(STORAGE_KEY, user);
  }
   getCustomer(){
        return this.storage.get("user");    
    }

    postAssignment(data,source_url,token) {
      var payload = {lesson_id:data.uploadData.lesson_id,course_id : data.uploadData.course_id,source_url:source_url}
      var httpOptions = {  headers: new Headers({'Content-Type':'application/json', 'Authorization':'Bearer '+ token}) };
     return this.http.post("https://arabic-classroom.com/wp-json/wp/v2/assignments",payload,httpOptions);
   }
}
