var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Http, Headers } from "@angular/http";
import { Environment } from "../environment/env";
var STORAGE_KEY = 'cartItems';
var httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
};
var CartService = (function () {
    function CartService(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    CartService.prototype.getCartItems = function () {
        return this.getAll();
    };
    CartService.prototype.addToCart = function (item) {
        var _this = this;
        debugger;
        return this.getAll().then(function (cartitems) {
            if (cartitems) {
                var flag = true;
                cartitems.forEach(function (element, i) {
                    if (element.product_id == item.product_id) {
                        cartitems[i].quantity = item.quantity;
                        flag = false;
                    }
                });
                if (flag) {
                    cartitems.push(item);
                }
                return _this.storage.set(STORAGE_KEY, cartitems);
            }
            else {
                return _this.storage.set(STORAGE_KEY, [item]);
            }
        });
    };
    CartService.prototype.removeFromCart = function (item) {
        var _this = this;
        return this.getAll().then(function (cartitems) {
            if (cartitems) {
                cartitems.forEach(function (element, i) {
                    if (element.product_id == item) {
                        var deletedItem = cartitems.splice(i, 1);
                    }
                });
                return _this.storage.set(STORAGE_KEY, cartitems);
            }
            else {
                return _this.storage.set(STORAGE_KEY, []);
            }
        });
    };
    CartService.prototype.updateCart = function (item, quantity) {
        var _this = this;
        return this.getAll().then(function (cartitems) {
            if (cartitems) {
                for (var i = 0; i < cartitems.length; i++) {
                    if (cartitems[i].product_id == item) {
                        cartitems[i].quantity = quantity;
                    }
                }
                return _this.storage.set(STORAGE_KEY, cartitems);
            }
            else {
                return _this.storage.set(STORAGE_KEY, []);
            }
        });
    };
    CartService.prototype.placeOrder = function (payload) {
        return this.http.post(Environment.URL + "api/order/create", payload, httpOptions);
    };
    CartService.prototype.emptyCart = function () {
        return this.storage.set(STORAGE_KEY, []);
    };
    CartService.prototype.getAll = function () {
        return this.storage.get(STORAGE_KEY);
    };
    CartService.prototype.getCustomer = function () {
        return this.storage.get("user");
    };
    return CartService;
}());
CartService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http, Storage])
], CartService);
export { CartService };
//# sourceMappingURL=cart-service.js.map