var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Storage } from '@ionic/storage';
var STORAGE_KEY = 'user';
var httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
};
var AuthenticationService = (function () {
    function AuthenticationService(http, storage) {
        this.storage = storage;
        this.http = http;
    }
    AuthenticationService.prototype.signin = function (email, password) {
        var body = { username: email, password: password };
        return this.http.post("https://arabic-classroom.com/wp-json/jwt-auth/v1/token", body);
    };
    AuthenticationService.prototype.setUser = function (user) {
        return this.storage.set(STORAGE_KEY, user);
    };
    AuthenticationService.prototype.getCustomer = function () {
        return this.storage.get("user");
    };
    AuthenticationService.prototype.postAssignment = function (data, source_url, token) {
        var payload = { lesson_id: data.uploadData.lesson_id, course_id: data.uploadData.course_id, source_url: source_url };
        var httpOptions = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token }) };
        return this.http.post("https://arabic-classroom.com/wp-json/wp/v2/assignments", payload, httpOptions);
    };
    return AuthenticationService;
}());
AuthenticationService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http, Storage])
], AuthenticationService);
export { AuthenticationService };
//# sourceMappingURL=authenticate-service.js.map