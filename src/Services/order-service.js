var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Environment } from "../environment/env";
import { Storage } from '@ionic/storage';
var STORAGE_KEY = 'user';
var httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
};
var OrderService = (function () {
    function OrderService(http, storage) {
        this.storage = storage;
        this.http = http;
    }
    OrderService.prototype.myorders = function (phone) {
        var payload = { phone: phone };
        return this.http.post(Environment.URL + "api/order/myorders", payload, httpOptions);
    };
    OrderService.prototype.getItems = function (id) {
        return this.http.get(Environment.URL + "api/order/orderitems?id=" + id, httpOptions);
    };
    OrderService.prototype.setUser = function (user) {
        return this.storage.set(STORAGE_KEY, user);
    };
    OrderService.prototype.getUser = function () {
        return this.storage.get(STORAGE_KEY);
    };
    return OrderService;
}());
OrderService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http, Storage])
], OrderService);
export { OrderService };
//# sourceMappingURL=order-service.js.map