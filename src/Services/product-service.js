var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/observable";
import { Environment } from "../environment/env";
import { Http, Headers, Response } from "@angular/http";
var httpOptions = {
    headers: new Headers({ 'Content-Type': 'application/json' })
};
var ProductService = (function () {
    function ProductService(http) {
        this.http = http;
    }
    ProductService.prototype.getallproducts = function (categoryId, page) {
        return this.http.get(Environment.URL + "api/product/category?id=" + categoryId + "&page=" + page);
    };
    ProductService.prototype.getcategoryDescription = function (categoryId) {
        return this.http.get(Environment.URL + "api/product/getcategorybyid?id=" + categoryId);
    };
    ProductService.prototype.searchProduct = function (keyword) {
        return this.http.get(Environment.URL + "api/product/search?keyword=" + keyword);
    };
    ProductService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    ProductService.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    };
    ProductService.prototype.getallCategoryImages = function (id) {
        var payload = { catid: id };
        return this.http.post(Environment.URL + "api/product/imagesbycategories", payload, httpOptions);
    };
    return ProductService;
}());
ProductService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http])
], ProductService);
export { ProductService };
//# sourceMappingURL=product-service.js.map