import {Injectable} from "@angular/core";
import {Observable} from "rxjs/observable";
import {Http,Headers,Response,RequestOptions} from "@angular/http";
import {Environment} from "../environment/environment";
import { Storage } from '@ionic/storage';


// const httpOptions =  {
//   headers: new Headers({ 'Content-Type':'application/json','Authorization':'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXJhYmljLWNsYXNzcm9vbS5jb20iLCJpYXQiOjE1Mjc3NjA3OTQsIm5iZiI6MTUyNzc2MDc5NCwiZXhwIjoxNTI4MzY1NTk0LCJkYXRhIjp7InVzZXIiOnsiaWQiOiIxMSJ9fX0.KT8ovxx6pDcKTbg1uLh51UGJCbZVNpRUN74s8i6XaFw' })
// }; 
@Injectable()

export class HomeService {
  
  private httpOptions:any;
 
  
  constructor(public http: Http,public storage:Storage){
   
  }

  getallAssignment(token) {      
      var httpOptions = {  headers: new Headers({'Content-Type':'application/json', 'Authorization':'Bearer '+ token}) };
      return this.http.get("https://arabic-classroom.com/wp-json/wp/v2/assignments",httpOptions);   
  }

  getNewAssignments(token) {      
      var httpOptions = {  headers: new Headers({'Content-Type':'application/json', 'Authorization':'Bearer '+ token}) };
      return this.http.get("https://arabic-classroom.com/wp-json/wp/v2/new_assignments",httpOptions);   
  }

  deleteAssignment(token,assignId,courseId,lessonId) {       
    var httpOptions = {  headers: new Headers({'Content-Type':'application/json', 'Authorization':'Bearer '+ token}) };
    return this.http.delete("https://arabic-classroom.com/wp-json/wp/v2/assignments/"+assignId+'?lesson_id='+lessonId+'&course_id='+courseId+'&force='+1,httpOptions);   
}


  getallAssignmentImages(id) {
     var payload = {assignid:id}
    return this.http.post(Environment.URL+"api/product/imagesbycategories",payload,this.httpOptions);
  }
  getAssignmentById(token){
    var httpOptions = {  headers: new Headers({'Content-Type':'application/json', 'Authorization':'Bearer '+ token}) };
    return this.http.get("https://arabic-classroom.com/wp-json/wp/v2/assignments",httpOptions);  
  }
}
   

   