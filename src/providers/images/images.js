var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Storage } from '@ionic/storage';
var ImagesProvider = (function () {
    function ImagesProvider(http, transfer, storage) {
        this.http = http;
        this.transfer = transfer;
        this.storage = storage;
        this.apiURL = 'https://arabic-classroom.com/wp-json/wp/v2/';
    }
    ImagesProvider.prototype.getImages = function () {
        return this.http.get(this.apiURL + 'media').map(function (res) { return res.json(); });
    };
    ImagesProvider.prototype.deleteImage = function (img) {
        return this.http.delete(this.apiURL + 'media/' + img._id);
    };
    ImagesProvider.prototype.uploadImage = function (img, token, cdata) {
        // Destination URL
        var url = this.apiURL + 'media';
        // File for Upload
        var targetPath = img;
        var options = {
            fileKey: 'file',
            fileName: cdata.fname,
            chunkedMode: false,
            params: { 'lesson_id': cdata.uploadData.lesson_id, "course_id": cdata.uploadData.course_id },
            httpMethod: 'POST',
            headers: { 'Authorization': "Bearer " + token, "Content-Disposition": "form-data; filename='" + cdata.fname + "'" }
        };
        var fileTransfer = this.transfer.create();
        // Use the FileTransfer to upload the image
        return fileTransfer.upload(targetPath, url, options);
    };
    return ImagesProvider;
}());
ImagesProvider = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Http, FileTransfer, Storage])
], ImagesProvider);
export { ImagesProvider };
//# sourceMappingURL=images.js.map