import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Storage } from '@ionic/storage';
 
@Injectable()
export class ImagesProvider {
  apiURL = 'https://arabic-classroom.com/wp-json/wp/v2/';
  token:string;
 
  constructor(public http: Http, private transfer: FileTransfer,public storage: Storage ) { 
   
  }
 
  getImages() {
    return this.http.get(this.apiURL + 'media').map(res => res.json());
  }
 
  deleteImage(img) {
    return this.http.delete(this.apiURL + 'media/' + img._id);
  }
 
  uploadImage(img, token,cdata) {

  
    // Destination URL
    let url = this.apiURL + 'media';
 
    // File for Upload
    var targetPath = img;
    
    var options: FileUploadOptions = {
      fileKey: 'file',
      fileName: cdata.fname,
      chunkedMode: false,
      params: { 'lesson_id': cdata.uploadData.lesson_id,"course_id":cdata.uploadData.course_id  },
      httpMethod: 'POST',
      headers:{'Authorization':"Bearer "+token,"Content-Disposition":"form-data; filename='"+cdata.fname+"'"}
      
    };
 
    const fileTransfer: FileTransferObject = this.transfer.create();
 
    // Use the FileTransfer to upload the image
    return fileTransfer.upload(targetPath, url, options);
  
  }
 
}