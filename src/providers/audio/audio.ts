import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Storage } from '@ionic/storage';
 
@Injectable()
export class AudioProvider {
  apiURL = 'https://arabic-classroom.com/wp-json/wp/v2/';
  token:string;
 
  constructor(public http: Http, private transfer: FileTransfer,public storage: Storage ) { 
   
  }
 
  getAudios() {
    return this.http.get(this.apiURL + 'media').map(res => res.json());
  }
 
  deleteAudio(img) {
    return this.http.delete(this.apiURL + 'media/' + img._id);
  }
 
  uploadAudio(aud, token,cdata,filename) {
    // Destination URL
    let url = this.apiURL + 'media';
 debugger;
    // File for Upload
    var targetPath = aud
    
    var options: FileUploadOptions = {
      fileKey: 'file',
      fileName: filename,
      chunkedMode: false,
      params: { 'lesson_id': cdata.uploadData.lesson_id,"course_id":cdata.uploadData.course_id},
      httpMethod: 'POST',
      headers:{'Authorization':"Bearer "+token,"Content-Disposition":"form-data; filename='"+filename+"'"}     
    };
 
    const fileTransfer: FileTransferObject = this.transfer.create();
 
    // Use the FileTransfer to upload the image
    return fileTransfer.upload(targetPath, url, options);
  
  }
 
}