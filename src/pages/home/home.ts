import {Component} from "@angular/core";
import {MenuController ,App} from "ionic-angular";
import {Storage} from '@ionic/storage';
import {BooksPage} from "../books/books";
import {NewAssignmentPage} from "../newassignment/newassignment";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
 

  constructor(private storage: Storage, public app: App ,public menu : MenuController) {
  }
   dodue() {
    this.app.getActiveNav().push(NewAssignmentPage);
   }
 

  // go to result page
   dobook() {
     debugger;
    this.app.getActiveNav().push(BooksPage);
  }

   ionViewDidEnter() {
    this.menu.swipeEnable(false);
}

ionViewWillLeave() {
  this.menu.swipeEnable(true);
 }    
}
