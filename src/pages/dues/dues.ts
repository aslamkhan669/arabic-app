import {Component} from "@angular/core";
import {Storage} from '@ionic/storage';
// import { Camera } from '@ionic-native/camera';
// import { ImagesProvider } from './../../providers/images/images';
import { IonicPage, NavController, ModalController, ActionSheetController } from 'ionic-angular';

import { UploadModalPage } from "./../upload-modal/upload-modal";
import { PreviewModalPage } from "./../preview-modal/preview-modal";
import {CameraPage} from "../camera/camera";
import {MicrophonePage} from "../microphone/microphone";

@Component({
  selector: 'page-dues',
  templateUrl: 'dues.html'
})
export class DuesPage {
  public dues: any;
  // images: any = [];

  constructor(private storage: Storage,public navCtrl: NavController, private modalCtrl: ModalController) {
  
  }

   micro(){
    this.navCtrl.push(MicrophonePage);
    }

   cam(){
    this.navCtrl.push(CameraPage)
   }
   public ionViewWillEnter(): void {
    this.navCtrl.swipeBackEnabled = true;
   }
    public ionViewDidLeave(): void {
    this.navCtrl.swipeBackEnabled = false;
   }
  //
  // 
  // reloadImages() {
  //   this.imagesProvider.getImages().subscribe(data => {
  //     this.images = data;
  //   });
  // }

  // openImage(img) {
  //   let modal = this.modalCtrl.create(PreviewModalPage, { img: img });
  //   modal.present();
  // }
   
  // presentActionSheet() {
  //   let actionSheet = this.actionSheetCtrl.create({
  //     title: 'Select Image Source',
  //     buttons: [
  //       {
  //         text: 'Load from Library',
  //         handler: () => {
  //           this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
  //         }
  //       },
  //       {
  //         text: 'Use Camera',
  //         handler: () => {
  //           this.takePicture(this.camera.PictureSourceType.CAMERA);
  //         }
  //       },
  //       {
  //         text: 'Cancel',
  //         role: 'cancel'
  //       }
  //     ]
  //   });
  //   actionSheet.present();
  // }
 
  // public takePicture(sourceType) {
  //   // Create options for the Camera Dialog
  //   var options = {
  //     quality: 100,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     sourceType: sourceType,
  //     saveToPhotoAlbum: false,
  //     correctOrientation: true
  //   };
 
  //   // Get the data of an image
  //   this.camera.getPicture(options).then((imagePath) => {
  //     let modal = this.modalCtrl.create(UploadModalPage, { data: imagePath });
  //     modal.present();
  //     modal.onDidDismiss(data => {
  //       if (data && data.reload) {
  //         this.reloadImages();
  //       }
  //     });
  //   }, (err) => {
  //     console.log('Error: ', err);
  //   });
  // }
  


}