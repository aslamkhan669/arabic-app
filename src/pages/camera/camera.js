var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { ImagesProvider } from './../../providers/images/images';
import { Component } from '@angular/core';
import { NavParams, NavController, LoadingController, ToastController, ModalController, Platform, ActionSheetController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { UploadModalPage } from "./../upload-modal/upload-modal";
import { PreviewModalPage } from "./../preview-modal/preview-modal";
// @IonicPage()
var CameraPage = (function () {
    function CameraPage(navCtrl, navParams, imagesProvider, platform, camera, actionSheetCtrl, modalCtrl, file, filePath, toastCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.imagesProvider = imagesProvider;
        this.platform = platform;
        this.camera = camera;
        this.actionSheetCtrl = actionSheetCtrl;
        this.modalCtrl = modalCtrl;
        this.file = file;
        this.filePath = filePath;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.images = [];
        this.lastImage = null;
        this.unit_name = this.navParams.data.unit_name;
        this.assignment_name = this.navParams.data.assign_name;
    }
    CameraPage.prototype.reloadImages = function () {
        var _this = this;
        this.imagesProvider.getImages().subscribe(function (data) {
            _this.images = data;
        });
    };
    CameraPage.prototype.deleteImage = function (img) {
        var _this = this;
        this.imagesProvider.deleteImage(img).subscribe(function (data) {
            _this.reloadImages();
        });
    };
    CameraPage.prototype.openImage = function (img) {
        var modal = this.modalCtrl.create(PreviewModalPage, { img: img });
        modal.present();
    };
    CameraPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            cssClass: 'action-sheets-basic-page',
            buttons: [
                {
                    text: 'Choose Camera',
                    icon: !this.platform.is('ios') ? 'camera' : null,
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Choose From Gallery',
                    icon: !this.platform.is('ios') ? 'image' : null,
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: !this.platform.is('ios') ? 'close' : null,
                }
            ]
        });
        actionSheet.present();
    };
    CameraPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        this.camera.getPicture(options).then(function (imagePath) {
            var name = _this.createFileName();
            var uploadData = _this.navParams.data;
            debugger;
            var modal = _this.modalCtrl.create(UploadModalPage, { data: imagePath, uploadData: uploadData, fname: name });
            //let modal = this.modalCtrl.create(UploadModalPage, { data: imagePath });
            modal.present();
            modal.onDidDismiss(function (data) {
                if (data && data.reload) {
                    _this.reloadImages();
                }
            });
        }, function (err) {
            console.log('Error: ', err);
        });
        // Get the data of an image
        // this.camera.getPicture(options).then((imagePath) => {
        //   // Special handling for Android library
        //   if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        //     this.filePath.resolveNativePath(imagePath)
        //       .then(filePath => {
        //         let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
        //         let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
        //         var name = this.createFileName();
        //         this.copyFileToLocalDir(correctPath, currentName, name);
        //         var tempPath = this.pathForImage(name);
        //         var uploadData = this.navParams.get("data");
        //         let modal = this.modalCtrl.create(UploadModalPage, { data: tempPath,uploadData:uploadData,fname:name });
        //         modal.present();
        //         modal.onDidDismiss(data => {
        //           if (data && data.reload) {
        //           this.reloadImages();
        //           }
        //         });
        //       });
        //   } else {
        //     var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        //     var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        //     this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        //   }
        // }, (err) => {
        //   this.presentToast('Error while selecting image.');
        // });
    };
    CameraPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    CameraPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    CameraPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    CameraPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    return CameraPage;
}());
CameraPage = __decorate([
    Component({
        selector: 'page-camera',
        templateUrl: 'camera.html',
    }),
    __metadata("design:paramtypes", [NavController, NavParams, ImagesProvider,
        Platform, Camera, ActionSheetController,
        ModalController,
        File, FilePath,
        ToastController, LoadingController])
], CameraPage);
export { CameraPage };
//# sourceMappingURL=camera.js.map