import { ImagesProvider } from './../../providers/images/images';
import { Component } from '@angular/core';
import { IonicPage, NavParams,NavController,LoadingController, ToastController,ModalController,Platform, ActionSheetController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { UploadModalPage } from "./../upload-modal/upload-modal";
import { PreviewModalPage } from "./../preview-modal/preview-modal";
import { NewAssignmentPage } from '../newassignment/newassignment';

 
declare var cordova: any;

// @IonicPage()
@Component({
  selector: 'page-camera',
  templateUrl: 'camera.html',
})
export class CameraPage {
  images: any = [];
  lastImage: string = null;
  loading: any;
  unit_name: string;
  assignment_name: string;

  constructor(public navCtrl: NavController,public navParams: NavParams, private imagesProvider: ImagesProvider, 
    public platform: Platform, private camera: Camera, private actionSheetCtrl: ActionSheetController,
     private modalCtrl: ModalController,
     private file: File, private filePath: FilePath,
     public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
       this.unit_name = this.navParams.data.unit_name;
       this.assignment_name  =this.navParams.data.assign_name;
  }
 
  reloadImages() {
    this.imagesProvider.getImages().subscribe(data => {
      this.images = data;
    });
  }
 
  deleteImage(img) {
    this.imagesProvider.deleteImage(img).subscribe(data => {
      this.reloadImages();
    });
  }
 
  openImage(img) {
    let modal = this.modalCtrl.create(PreviewModalPage, { img: img });
    modal.present();
  }
 
  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Choose Camera',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },       
        {       
          text: 'Choose From Gallery',
          icon: !this.platform.is('ios') ? 'image' : null,
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,

        }
      ]
    });
    actionSheet.present();
  }
 
  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      allowEdit:true,
      targetHeight: 400,
      targetwidth : 400,
      sourceType: sourceType,
      saveToPhotoAlbum: true,
      correctOrientation: true
    };
    this.camera.getPicture(options).then((imagePath) => {
      var name = this.createFileName();
      var uploadData = this.navParams.data;
      debugger;
      let modal = this.modalCtrl.create(UploadModalPage, { data: imagePath,uploadData:uploadData,fname:name });
      //let modal = this.modalCtrl.create(UploadModalPage, { data: imagePath });
      modal.present();
      modal.onDidDismiss(data => {
        if (data && data.reload) {
          this.reloadImages();
        }
      });
    }, (err) => {
      console.log('Error: ', err);
    });
   
    // Get the data of an image
    // this.camera.getPicture(options).then((imagePath) => {
    //   // Special handling for Android library
    //   if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
    //     this.filePath.resolveNativePath(imagePath)
    //       .then(filePath => {
    //         let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
    //         let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
    //         var name = this.createFileName();
    //         this.copyFileToLocalDir(correctPath, currentName, name);
    //         var tempPath = this.pathForImage(name);
    //         var uploadData = this.navParams.get("data");
            
    //         let modal = this.modalCtrl.create(UploadModalPage, { data: tempPath,uploadData:uploadData,fname:name });
    //         modal.present();
    //         modal.onDidDismiss(data => {
    //           if (data && data.reload) {
    //           this.reloadImages();
    //           }
    //         });
            
            
            
    //       });
    //   } else {
    //     var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
    //     var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
    //     this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    //   }
    // }, (err) => {
    //   this.presentToast('Error while selecting image.');
    // });
  }
  private createFileName() {
    var d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";
    return newFileName;
  }
   
  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }
   
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
   
  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }
  goToDueAssignments(){
    this.navCtrl.push(NewAssignmentPage);
    }



  // public takePicture(sourceType) {
  //   // Create options for the Camera Dialog
  //   var options = {
  //     quality: 100,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     sourceType: sourceType,
  //     saveToPhotoAlbum: false,
  //     correctOrientation: true
  //   };
 
  //   //Get the data of an image
  //   this.camera.getPicture(options).then((imagePath) => {
  //     debugger;
  //     let modal = this.modalCtrl.create(UploadModalPage, { data: imagePath });
  //     modal.present();
  //     modal.onDidDismiss(data => {
  //       if (data && data.reload) {
  //         this.reloadImages();
  //       }
  //     });
  //   }, (err) => {
  //     console.log('Error: ', err);
  //   });
  // }
}