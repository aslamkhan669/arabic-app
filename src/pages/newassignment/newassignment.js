var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { Storage } from '@ionic/storage';
import { LoadingController, NavController } from "ionic-angular";
import { HomeService } from "../../services/home-service";
import { CameraPage } from "../camera/camera";
import { MicrophonePage } from "../microphone/microphone";
import { NewAssignment_DetailsPage } from "../newassignment_details/newassignment_details";
import { AuthenticationService } from '../../Services/authenticate-service';
var NewAssignmentPage = (function () {
    function NewAssignmentPage(auth, storage, loading, navCtrl, userAssignment) {
        this.auth = auth;
        this.storage = storage;
        this.loading = loading;
        this.navCtrl = navCtrl;
        this.userAssignment = userAssignment;
        this.images = [];
        this.new_assignments = [];
        this.page = 1;
        this.hasScrollEnded = false;
        this.doscroll = true;
        this.errorMessage = "";
        this.initializeData();
    }
    NewAssignmentPage.prototype.initializeData = function () {
        var _this = this;
        this.auth.getCustomer().then(function (data) {
            _this.userAssignment.getNewAssignments(data.token).subscribe(function (res) {
                var dueAssign = res.json();
                _this.userAssignment.getallAssignment(data.token).subscribe(function (res) {
                    var pastAssignments = res.json();
                    var pastAssignmentsWithOpenStatus = pastAssignments.filter(function (_a) {
                        var details = _a.details;
                        return details.status == "open";
                    });
                    var allassignments = pastAssignmentsWithOpenStatus.concat(dueAssign);
                    _this.new_assignments = allassignments;
                });
            });
        });
    };
    // view camera
    NewAssignmentPage.prototype.viewCamera = function (id, courseName, CID, LID, unitName, assignmentName) {
        var data = { assignmentId: id, course_id: CID, lesson_id: LID, unit_name: unitName, assign_name: assignmentName };
        this.navCtrl.push(CameraPage, data);
    };
    // view microphone
    NewAssignmentPage.prototype.viewMicrophone = function (id, courseName, CID, LID, unitName, assignmentName) {
        var data = { ID: id, assignmentId: id, course_id: CID, lesson_id: LID, unit_name: unitName, assign_name: assignmentName };
        this.navCtrl.push(MicrophonePage, data);
    };
    NewAssignmentPage.prototype.viewSubmitAssignment = function (lessonID) {
        var _this = this;
        this.auth.getCustomer().then(function (data) {
            _this.userAssignment.getallAssignment(data.token).subscribe(function (res) {
                debugger;
                var pastAssignWithOpenStatus = res.json();
                var newassignment_details_data = pastAssignWithOpenStatus.filter(function (_a) {
                    var details = _a.details;
                    return details.lesson.ID == lessonID;
                });
                if (newassignment_details_data.length == 0) {
                    newassignment_details_data.IsNotSubmitted = true;
                    _this.navCtrl.push(NewAssignment_DetailsPage, newassignment_details_data);
                }
                else {
                    _this.navCtrl.push(NewAssignment_DetailsPage, newassignment_details_data);
                }
            });
        });
    };
    NewAssignmentPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        setTimeout(function () {
            if (!_this.hasScrollEnded) {
                _this.auth.getCustomer().then(function (data) {
                    _this.userAssignment.getNewAssignments(data.token).subscribe(function (res) {
                        if (res.ok && res.json().length > 0) {
                            _this.new_assignments = res.json();
                            _this.page = _this.page + 1;
                        }
                        else {
                            _this.hasScrollEnded = true;
                        }
                    }, function (error) { return _this.errorMessage = error; });
                });
            }
            if (infiniteScroll != null)
                infiniteScroll.complete();
        }, 1000);
    };
    return NewAssignmentPage;
}());
NewAssignmentPage = __decorate([
    Component({
        selector: 'page-newassignment',
        templateUrl: 'newassignment.html'
    }),
    __metadata("design:paramtypes", [AuthenticationService, Storage, LoadingController, NavController, HomeService])
], NewAssignmentPage);
export { NewAssignmentPage };
//# sourceMappingURL=newassignment.js.map