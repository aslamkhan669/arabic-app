import {Component} from "@angular/core";
import {Storage} from '@ionic/storage';
import {LoadingController,NavController, Loading} from "ionic-angular";
import {HomeService} from "../../services/home-service";
import {Environment} from "../../environment/environment";
import {CameraPage} from "../camera/camera";
import {MicrophonePage} from "../microphone/microphone";
import {NewAssignment_DetailsPage} from "../newassignment_details/newassignment_details";
import {AuthenticationService} from '../../Services/authenticate-service';
import { HomePage } from "../home/home";


@Component({
  selector: 'page-newassignment',
  templateUrl: 'newassignment.html'
})
export class NewAssignmentPage {
  public images = [];
  public books: any;
  public new_assignments = [];
  public assign : any;
  public URL:string;
  public page = 1;
  public hasScrollEnded = false;
  public doscroll = true;
  public errorMessage = "";
  public dataToMove: any;
  public tempdata: any;

  constructor(private auth:AuthenticationService, private storage: Storage,public loading: LoadingController,public navCtrl: NavController,private userAssignment:HomeService) {
   this.initializeData();
  }
  
  initializeData(){
    this.auth.getCustomer().then(data=>{
              this.userAssignment.getNewAssignments(data.token).subscribe(res=>{
                var dueAssign = res.json();
                dueAssign.forEach(e => {
                  e["sortid"] = e.id;
                });
        this.userAssignment.getallAssignment(data.token).subscribe(res=>{
                var pastAssignments = res.json();
                var pastAssignmentsWithOpenStatus = pastAssignments.filter(({details}) => details.status == "open" );
                pastAssignmentsWithOpenStatus.forEach(e => {
                  e["sortid"] = e.details.lesson.ID;
                });
                var allassignments = pastAssignmentsWithOpenStatus.concat(dueAssign);
                this.new_assignments = this.sortJSONData(allassignments,"sortid","asc");
                debugger;
        });
        });
    })     
  }

// sort json data in required order
 sortJSONData(data, key, order) {
    return data.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        if (order === 'asc' ) { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
        if (order === 'dec') { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
    });
}
  // view camera
  viewCamera(id:Number,courseName:string,CID:string,LID:string,unitName: string,assignmentName:string){
    this.auth.getCustomer().then(data=>{      
      this.userAssignment.getallAssignment(data.token).subscribe(res=>{
           var pastAssignWithOpenStatus = res.json();          
          var newassignment_details_data = pastAssignWithOpenStatus.filter(({details}) => details.lesson.ID == LID ); 
          if(newassignment_details_data.length == 0){
            var camData = {assignmentId : id,course_id:CID,lesson_id:LID,unit_name:unitName,assign_name:assignmentName};
            this.navCtrl.push(CameraPage,camData);           }
           else
           {this.navCtrl.push(NewAssignment_DetailsPage,newassignment_details_data); }
        });
  })
    // var camData = {assignmentId : id,course_id:CID,lesson_id:LID,unit_name:unitName,assign_name:assignmentName};
    // this.navCtrl.push(CameraPage,camData);
  }


  // view microphone
  viewMicrophone(id:Number,courseName:string,CID:string,LID:string,unitName: string,assignmentName:string){
    this.auth.getCustomer().then(data=>{      
      this.userAssignment.getallAssignment(data.token).subscribe(res=>{
           var pastAssignWithOpenStatus = res.json();          
          var newassignment_details_data = pastAssignWithOpenStatus.filter(({details}) => details.lesson.ID == LID ); 
          if(newassignment_details_data.length == 0){
            var audData = {assignmentId : id,course_id:CID,lesson_id:LID,unit_name:unitName,assign_name:assignmentName};
            this.navCtrl.push(MicrophonePage,audData);           }
           else
           {this.navCtrl.push(NewAssignment_DetailsPage,newassignment_details_data); }
        });
  })  
    
    // var data = {ID:id,assignmentId : id,course_id:CID,lesson_id:LID,unit_name:unitName,assign_name:assignmentName};
    //   this.navCtrl.push(MicrophonePage,data);
    }
// navigate to home page anytime 
  goToHomePage(){
    this.navCtrl.push(HomePage);
      this.navCtrl.setRoot(HomePage);
    }


    viewSubmitAssignment(lessonID:Number){      
      this.auth.getCustomer().then(data=>{      
          this.userAssignment.getallAssignment(data.token).subscribe(res=>{
               var pastAssignWithOpenStatus = res.json();          
              var newassignment_details_data = pastAssignWithOpenStatus.filter(({details}) => details.lesson.ID == lessonID ); 
              if(newassignment_details_data.length == 0){newassignment_details_data.IsNotSubmitted =true;this.navCtrl.push(NewAssignment_DetailsPage,newassignment_details_data); }else{this.navCtrl.push(NewAssignment_DetailsPage,newassignment_details_data); }
          });
      })     
    }

    doInfinite(infiniteScroll) {   
      setTimeout(() => {     
        if(!this.hasScrollEnded){
          this.auth.getCustomer().then(data=>{
          this.userAssignment.getNewAssignments(data.token).subscribe(
             res => {      
               debugger;   
              if(res.ok && res.json().length>0){
                this.new_assignments = res.json();            
                this.page = this.page+1;
               }
               else{
                 this.hasScrollEnded = true;
               }
             },
             error =>  this.errorMessage = <any>error);
        }
      )} 
        if(infiniteScroll!=null)
            infiniteScroll.complete();      
      }, 1000);
    }
}
