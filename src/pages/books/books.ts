import {Component} from "@angular/core";
import {LoadingController,App} from "ionic-angular";
import {HomeService} from "../../services/home-service";
import {BookDetailPage} from "../book-detail/book-detail";
import {AuthenticationService} from '../../Services/authenticate-service';
import {PhotoViewer} from '@ionic-native/photo-viewer';

@Component({
  selector: 'page-books',
  templateUrl: 'books.html'
})
export class BooksPage {
  public images = [];
  public books: any;
  public assignments:any;
  public assign : any;
  public URL:string;
  public page = 1;
  public hasScrollEnded = false;
  public doscroll = true;
  public errorMessage = "";

  constructor(private auth:AuthenticationService,public photoViewer : PhotoViewer,public loading: LoadingController,private userAssignment:HomeService,private app : App) {
   this.initializeData();
  }
  
  initializeData(){
    this.auth.getCustomer().then(data=>{
       this.userAssignment.getallAssignment(data.token).subscribe(res=>{
          var data=(res.json()).filter(({details}) => details.status !== "open" );
            this.assignments = this.sortJSONData(data,"id","asc");        
      });
    })    
  }
  sortJSONData(data, key, order) {
    return data.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        if (order === 'asc' ) { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
        if (order === 'dec') { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
    });
}

  showImage(img){
    this.photoViewer.show(img);
  }

  // view assignment detail
  view(id:Number,courseName:string,unitName:string,lessonName:string,CID:string,LID:string,status:string,file_link:string,feedback:string,assign_type:string){
    var data = {assignmentId : id,courseName:courseName,unitName:unitName,lessonName:lessonName,course_id:CID,lesson_id:LID,Status:status,file_link:file_link,feedback:feedback,assignment_type:assign_type};
    this.app.getActiveNav().push(BookDetailPage,data);
  }

  doInfinite(infiniteScroll) {   
    setTimeout(() => {     
      if(!this.hasScrollEnded){
        this.auth.getCustomer().then(data=>{
        this.userAssignment.getallAssignment(data.token).subscribe(
           res => {         
            if(res.ok && res.json().length>0){
              this.assignments = res.json();            
              this.page = this.page+1;
             }
             else{
               this.hasScrollEnded = true;
             }
           },
           error =>  this.errorMessage = <any>error);
      }
    )} 
      if(infiniteScroll!=null)
          infiniteScroll.complete();      
    }, 1000);
  }
}
