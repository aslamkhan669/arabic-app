var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { Storage } from '@ionic/storage';
import { LoadingController, NavController } from "ionic-angular";
import { HomeService } from "../../services/home-service";
import { BookDetailPage } from "../book-detail/book-detail";
import { AuthenticationService } from '../../Services/authenticate-service';
import { PhotoViewer } from '@ionic-native/photo-viewer';
var BooksPage = (function () {
    function BooksPage(auth, photoViewer, storage, loading, nav, userAssignment) {
        this.auth = auth;
        this.photoViewer = photoViewer;
        this.storage = storage;
        this.loading = loading;
        this.nav = nav;
        this.userAssignment = userAssignment;
        this.images = [];
        this.page = 1;
        this.hasScrollEnded = false;
        this.doscroll = true;
        this.errorMessage = "";
        this.initializeData();
    }
    BooksPage.prototype.initializeData = function () {
        var _this = this;
        this.auth.getCustomer().then(function (data) {
            _this.userAssignment.getallAssignment(data.token).subscribe(function (res) {
                debugger;
                var data = (res.json()).filter(function (_a) {
                    var details = _a.details;
                    return details.status !== "open";
                });
                _this.assignments = data;
            });
        });
    };
    BooksPage.prototype.showImage = function (img) {
        this.photoViewer.show(img);
    };
    // view assignment detail
    BooksPage.prototype.view = function (id, courseName, unitName, lessonName, CID, LID, status, file_link, feedback) {
        var data = { assignmentId: id, courseName: courseName, unitName: unitName, lessonName: lessonName, course_id: CID, lesson_id: LID, Status: status, file_link: file_link, feedback: feedback };
        this.nav.push(BookDetailPage, data);
    };
    BooksPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        setTimeout(function () {
            if (!_this.hasScrollEnded) {
                _this.auth.getCustomer().then(function (data) {
                    _this.userAssignment.getallAssignment(data.token).subscribe(function (res) {
                        if (res.ok && res.json().length > 0) {
                            _this.assignments = res.json();
                            _this.page = _this.page + 1;
                        }
                        else {
                            _this.hasScrollEnded = true;
                        }
                    }, function (error) { return _this.errorMessage = error; });
                });
            }
            if (infiniteScroll != null)
                infiniteScroll.complete();
        }, 1000);
    };
    return BooksPage;
}());
BooksPage = __decorate([
    Component({
        selector: 'page-books',
        templateUrl: 'books.html'
    }),
    __metadata("design:paramtypes", [AuthenticationService, PhotoViewer, Storage, LoadingController, NavController, HomeService])
], BooksPage);
export { BooksPage };
//# sourceMappingURL=books.js.map