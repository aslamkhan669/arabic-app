import {Component} from "@angular/core";
import { AudioProvider } from './../../providers/audio/audio';
import {NavController,ToastController,NavParams,LoadingController, Platform} from "ionic-angular";
import { File } from '@ionic-native/file';
import {AuthenticationService} from '../../Services/authenticate-service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Media, MediaObject } from '@ionic-native/media';
import { BooksPage } from '../books/books';
import { NewAssignmentPage } from "../newassignment/newassignment";

@Component({
  selector: 'page-microphone',
  templateUrl: 'microphone.html'
})
export class MicrophonePage {
  apiURL = 'https://arabic-classroom.com/wp-json/wp/v2/';
  token:string;
  recording: boolean = false;
  playing: any[] =[];
  filePath: string;
  fileName: string;
  audList: string;
  audio: MediaObject;
  audioList: any[] = [];
  audioData: any;
  cdata: any;
  pauseandplay: any;
  audioSpecificToAssignment: any;
  unit_name:string;
  assignment_name:string;


  constructor(public navCtrl: NavController,
    private media: Media,
    private auth:AuthenticationService,
    public loadingCtrl: LoadingController,
    private file: File,
    public toastCtrl: ToastController,
    private audioProvider: AudioProvider,
    private transfer: FileTransfer,
    public navParams: NavParams,
    public platform: Platform) {
      this.unit_name = this.navParams.data.unit_name;
      this.assignment_name = this.navParams.data.assign_name;
    }

  ionViewWillEnter() {
    //this.getAudioList();
  }

  getAudioList() {
    if(localStorage.getItem("audiolist")) {
      this.audioList = JSON.parse(localStorage.getItem("audiolist")).filter(a => a.id === this.navParams.data.ID);
      
      console.log(this.audioList);
    }
  }

  startRecord() {
    if (this.platform.is('ios')) {
      this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.m4a';
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
    } else if (this.platform.is('android')) {
      this.fileName = 'record'+new Date().getDate()+new Date().getMonth()+new Date().getFullYear()+new Date().getHours()+new Date().getMinutes()+new Date().getSeconds()+'.mp3';
      this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
    }
    this.audio.startRecord();
    this.recording = true;
  }

  stopRecord() {
    this.audio.stopRecord();
    var uploadData = this.navParams.data;
    let data = { filename: this.fileName,id:uploadData.ID };
    this.audioList.push(data);
    localStorage.setItem("audiolist", JSON.stringify(this.audioList));
    this.recording = false;
    this.getAudioList();
  }

  playAudio(file,idx) {
    if (this.platform.is('ios')) {
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + file;
      this.audio = this.media.create(this.filePath);
    } else if (this.platform.is('android')) {
      this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + file;
      this.audio = this.media.create(this.filePath);
    }
    this.playing[idx] = true;
    this.audio.play();
    this.audio.setVolume(1.0);
    this.pauseandplay = idx;
    setTimeout(() => { debugger;this.playing[this.pauseandplay] = false ; }, Math.ceil(this.audio.getDuration())*1000);
   
  }


  deleteAudio(file,idx) {
    var audios = JSON.parse(localStorage.getItem('audiolist')); 
    audios.splice(idx,1);
    this.audioList = audios;  
    localStorage.setItem('audiolist',JSON.stringify(audios));
    this.navCtrl.push(MicrophonePage);
  }
  stopAudio(file,idx) {
    this.audio.stop();
    this.playing[idx] = false;
  }
  saveAudio(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();

    this.auth.getCustomer().then(user=>{
      var token = user.token;
      var fileLength = this.filePath.split('/').length;
      var filename = ((this.filePath.split('/')[fileLength-1]));
      var data = this.navParams.data;
      debugger;
      this.audioProvider.uploadAudio(this.filePath,user.token,{uploadData:data},filename).then(data => {
        //this.loading.dismissAll()
        debugger;
        var sourceUrl = (JSON.parse(data.response)).source_url;
        var datatoupload = this.navParams.data;
        this.auth.postAssignment({uploadData:datatoupload},sourceUrl,token).subscribe(res=>{
          console.log(res);
          loading.dismiss();

              this.presentToast('Assignment uploaded successfully !!!','center');
              this.navCtrl.push(NewAssignmentPage);
        })
        console.log(data);
        this.presentToast('Audio successfully uploaded.','bottom');
      }, err => {
       // this.loading.dismissAll()
        this.presentToast('Error while uploading file.','bottom');
        loading.dismiss();
      });
    })
  }

  private presentToast(text,pos) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1000,
      position: pos
    });
    toast.present();
  }
  goToDueAssignments(){
    this.navCtrl.push(NewAssignmentPage);
    }
}
