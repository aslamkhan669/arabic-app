var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { AudioProvider } from './../../providers/audio/audio';
import { NavController, ToastController, NavParams, LoadingController, Platform } from "ionic-angular";
import { File } from '@ionic-native/file';
import { AuthenticationService } from '../../Services/authenticate-service';
import { FileTransfer } from '@ionic-native/file-transfer';
import { Media } from '@ionic-native/media';
import { BooksPage } from '../books/books';
var MicrophonePage = MicrophonePage_1 = (function () {
    function MicrophonePage(navCtrl, media, auth, loadingCtrl, file, toastCtrl, audioProvider, transfer, navParams, platform) {
        this.navCtrl = navCtrl;
        this.media = media;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.file = file;
        this.toastCtrl = toastCtrl;
        this.audioProvider = audioProvider;
        this.transfer = transfer;
        this.navParams = navParams;
        this.platform = platform;
        this.apiURL = 'https://arabic-classroom.com/wp-json/wp/v2/';
        this.recording = false;
        this.playing = [];
        this.audioList = [];
        this.unit_name = this.navParams.data.unit_name;
        this.assignment_name = this.navParams.data.assign_name;
    }
    MicrophonePage.prototype.ionViewWillEnter = function () {
        this.getAudioList();
    };
    MicrophonePage.prototype.getAudioList = function () {
        var _this = this;
        if (localStorage.getItem("audiolist")) {
            this.audioList = JSON.parse(localStorage.getItem("audiolist")).filter(function (a) { return a.id === _this.navParams.data.ID; });
            console.log(this.audioList);
        }
    };
    MicrophonePage.prototype.startRecord = function () {
        debugger;
        if (this.platform.is('ios')) {
            this.fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.m4a';
            this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
            this.audio = this.media.create(this.filePath);
        }
        else if (this.platform.is('android')) {
            this.fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.3gp';
            this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
            this.audio = this.media.create(this.filePath);
        }
        this.audio.startRecord();
        this.recording = true;
    };
    MicrophonePage.prototype.stopRecord = function () {
        debugger;
        this.audio.stopRecord();
        var uploadData = this.navParams.data;
        var data = { filename: this.fileName, id: uploadData.ID };
        this.audioList.push(data);
        localStorage.setItem("audiolist", JSON.stringify(this.audioList));
        this.recording = false;
        this.getAudioList();
    };
    MicrophonePage.prototype.playAudio = function (file, idx) {
        var _this = this;
        debugger;
        if (this.platform.is('ios')) {
            this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + file;
            this.audio = this.media.create(this.filePath);
        }
        else if (this.platform.is('android')) {
            this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + file;
            this.audio = this.media.create(this.filePath);
        }
        this.playing[idx] = true;
        this.audio.play();
        this.audio.setVolume(1.0);
        this.pauseandplay = idx;
        setTimeout(function () { debugger; _this.playing[_this.pauseandplay] = false; }, Math.ceil(this.audio.getDuration()) * 1000);
    };
    MicrophonePage.prototype.deleteAudio = function (file, idx) {
        debugger;
        var audios = JSON.parse(localStorage.getItem('audiolist'));
        audios.splice(idx, 1);
        this.audioList = audios;
        localStorage.setItem('audiolist', JSON.stringify(audios));
        this.navCtrl.push(MicrophonePage_1);
    };
    MicrophonePage.prototype.stopAudio = function (file, idx) {
        this.audio.stop();
        this.playing[idx] = false;
    };
    MicrophonePage.prototype.saveAudio = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.auth.getCustomer().then(function (user) {
            debugger;
            var token = user.token;
            var filename = ((_this.filePath.split('/')[((_this.filePath.split("/"))).length]));
            var data = _this.navParams.data;
            _this.audioProvider.uploadAudio(_this.filePath, user.token, { uploadData: data }, filename).then(function (data) {
                //this.loading.dismissAll()
                debugger;
                var sourceUrl = (JSON.parse(data.response)).source_url;
                var datatoupload = _this.navParams.data;
                _this.auth.postAssignment({ uploadData: datatoupload }, sourceUrl, token).subscribe(function (res) {
                    console.log(res);
                    loading.dismiss();
                    _this.presentToast('Assignment uploaded successfully !!!');
                    _this.navCtrl.push(BooksPage);
                });
                console.log(data);
                _this.presentToast('Audio successfully uploaded.');
            }, function (err) {
                // this.loading.dismissAll()
                _this.presentToast('Error while uploading file.');
                loading.dismiss();
            });
        });
    };
    MicrophonePage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    return MicrophonePage;
}());
MicrophonePage = MicrophonePage_1 = __decorate([
    Component({
        selector: 'page-microphone',
        templateUrl: 'microphone.html'
    }),
    __metadata("design:paramtypes", [NavController,
        Media,
        AuthenticationService,
        LoadingController,
        File,
        ToastController,
        AudioProvider,
        FileTransfer,
        NavParams,
        Platform])
], MicrophonePage);
export { MicrophonePage };
var MicrophonePage_1;
//# sourceMappingURL=microphone.js.map