var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { NavController, NavParams, ToastController } from "ionic-angular";
import { HomeService } from "../../services/home-service";
import { AuthenticationService } from "../../Services/authenticate-service";
import { BooksPage } from "../books/books";
import { PhotoViewer } from '@ionic-native/photo-viewer';
var NewAssignment_DetailsPage = (function () {
    function NewAssignment_DetailsPage(nav, photoViewer, toastCtrl, auth, navParams, userAssignment) {
        this.nav = nav;
        this.photoViewer = photoViewer;
        this.toastCtrl = toastCtrl;
        this.auth = auth;
        this.navParams = navParams;
        this.userAssignment = userAssignment;
        this.IsNotSubmitted = false;
        this.initializeData();
    }
    NewAssignment_DetailsPage.prototype.initializeData = function () {
        debugger;
        if ((this.navParams['data'].length != 0)) {
            var data = this.navParams['data'][0];
            this.assignmentId = data.id;
            this.assignment_post_name = data.details.course.post_name;
            this.lessonName = data.details.lesson.post_name;
            this.unitName = data.details.unit.post_name;
            this.course_id = data.details.course.ID;
            this.course_name = data.details.course.post_name;
            this.lesson_id = data.details.lesson.ID;
            this.status = data.details.status;
            this.file_link = data.details.file_link;
        }
        else {
            this.IsNotSubmitted = true;
        }
    };
    NewAssignment_DetailsPage.prototype.deleteAssignment = function (assignmentId, lessonId, courseId) {
        var _this = this;
        debugger;
        this.auth.getCustomer().then(function (data) {
            debugger;
            _this.userAssignment.deleteAssignment(data.token, _this.assignmentId, _this.course_id, _this.lesson_id).subscribe(function (res) {
                _this.presentToast('Assignment deleted!!!');
                _this.nav.push(BooksPage);
            });
        });
    };
    NewAssignment_DetailsPage.prototype.showImage = function (img) {
        this.photoViewer.show(img);
    };
    NewAssignment_DetailsPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    };
    return NewAssignment_DetailsPage;
}());
NewAssignment_DetailsPage = __decorate([
    Component({
        selector: 'page-book-detail',
        templateUrl: 'newassignment_details.html'
    }),
    __metadata("design:paramtypes", [NavController, PhotoViewer, ToastController, AuthenticationService, NavParams, HomeService])
], NewAssignment_DetailsPage);
export { NewAssignment_DetailsPage };
//# sourceMappingURL=newassignment_details.js.map