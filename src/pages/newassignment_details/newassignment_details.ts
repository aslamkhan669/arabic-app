import {Component} from "@angular/core";
import {NavController, NavParams, ToastController,Platform} from "ionic-angular";
import {HomeService} from "../../services/home-service";
import { AuthenticationService } from "../../Services/authenticate-service";
import {Environment} from "../../environment/environment";
import { BooksPage } from "../books/books";
import {PhotoViewer} from '@ionic-native/photo-viewer';
import { FileTransferError } from "../../../node_modules/@ionic-native/file-transfer";
import { NewAssignmentPage } from "../newassignment/newassignment";
import { CameraPage } from "../camera/camera";
import { File } from '@ionic-native/file';
import { Media, MediaObject } from '@ionic-native/media';
import { MicrophonePage } from "../microphone/microphone";


@Component({
  selector: 'page-book-detail',
  templateUrl: 'newassignment_details.html'
})


export class NewAssignment_DetailsPage {
  debugger;
  public books: any;
  public assignment_name : any;
  public assignment_post_name:any;
  public assignmentId:any;
  public unitName:any;
  public lessonName:any;
  public assignment:any;
  public course_id:any;
  public course_name:any;
  public lesson_id:any;
  public status:any;
  public URL:string;
  public file_link : string;
  public IsNotSubmitted =false;
  public assignment_type : string;
  playing: any[] =[];
  audio: MediaObject;
  audioList: any[] = [];
  filePath: string;
  fileName: string;

  constructor(public nav: NavController,public photoViewer : PhotoViewer,private file: File,public toastCtrl:ToastController,public auth :AuthenticationService, public navParams: NavParams,public userAssignment : HomeService,public platform:Platform,private media: Media) {
      this.initializeData();
  }

  initializeData(){
    if((this.navParams['data'].length != 0))
    {
    var data = this.navParams['data'][0];
    this.assignmentId = data.id;
    this.assignment_post_name = data.details.course.post_title;
    this.lessonName = data.details.lesson.post_title;
    this.unitName = data.details.unit.post_title;
    this.course_id = data.details.course.ID;
    this.course_name = data.details.course.post_title;
    this.lesson_id = data.details.lesson.ID;
    this.status = data.details.status;
    this.file_link  = data.details.file_link;
    this.assignment_type  = data.details.assignment_type;
  }
  else{this.IsNotSubmitted = true;}
}

   deleteAssignment(assignmentId:Number,lessonId:Number,courseId:Number,assignName:string,unitName:string){
          this.auth.getCustomer().then(data=>{
            debugger;
             this.userAssignment.deleteAssignment(data.token,this.assignmentId,this.course_id,this.lesson_id).subscribe(res=>{
            this.presentToast('Assignment deleted!!!');
            if(this.assignment_type == "image")
            {
            var camData = {assignmentId : assignmentId,course_id:courseId,lesson_id:lessonId,unit_name:unitName,assign_name:assignName};
              this.nav.push(CameraPage,camData);
            }
            else if(this.assignment_type == "audio")
            {
              var audData = {assignmentId : assignmentId,course_id:courseId,lesson_id:lessonId,unit_name:unitName,assign_name:assignName};
              this.nav.push(MicrophonePage,audData);
            }
            });
          }) 
   }

   showImage(img){
    this.photoViewer.show(img);
  }

   private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  listenAudio(file,idx,e:Event){
    this.debugger;
    e.preventDefault();
    e.stopImmediatePropagation();
    if (this.platform.is('ios')) {
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + file;
      this.audio = this.media.create(this.filePath);
    } else if (this.platform.is('android')) {
      // this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + file;
      this.filePath = file;
      this.audio = this.media.create(this.filePath);
    }
    this.playing[idx] = true;
    this.audio.play();
    this.audio.setVolume(1.0);
    }

    stopAudio(file,idx) {
      this.audio.stop();
      this.playing[idx] = false;
    }
    stopAudioAtLeavingPage(){
      this.nav.push(NewAssignmentPage);
      this.audio.stop();
      this.playing[0] = false;
      }

}
