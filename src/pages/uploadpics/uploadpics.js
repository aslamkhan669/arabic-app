var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { Storage } from '@ionic/storage';
import { LoadingController, NavController } from "ionic-angular";
import { HomeService } from "../../services/home-service";
import { Environment } from "../../environment/environment";
import { BookDetailPage } from "../book-detail/book-detail";
import { AuthenticationService } from '../../Services/authenticate-service';
var UploadpicsPage = (function () {
    //public loader:any;
    function UploadpicsPage(auth, storage, loading, nav, userAssignment) {
        this.auth = auth;
        this.storage = storage;
        this.loading = loading;
        this.nav = nav;
        this.userAssignment = userAssignment;
        this.images = [];
        this.initializeData();
    }
    UploadpicsPage.prototype.initializeData = function () {
        var _this = this;
        this.auth.getCustomer().then(function (data) {
            _this.URL = Environment.URL + "wp/v2/assignments";
            _this.userAssignment.getallAssignment(data.token).subscribe(function (res) {
                _this.assignments = res.json();
            });
        });
    };
    // view book detail
    UploadpicsPage.prototype.view = function (id, courseName, unitName, lessonName, CID, LID) {
        debugger;
        var data = { assignmentId: id, courseName: courseName, unitName: unitName, lessonName: lessonName, course_id: CID, lesson_id: LID };
        this.nav.push(BookDetailPage, data);
    };
    return UploadpicsPage;
}());
UploadpicsPage = __decorate([
    Component({
        selector: 'page-upload',
        templateUrl: 'uploadpics.html'
    }),
    __metadata("design:paramtypes", [AuthenticationService, Storage, LoadingController, NavController, HomeService])
], UploadpicsPage);
export { UploadpicsPage };
//# sourceMappingURL=uploadpics.js.map