import {Component} from "@angular/core";
import {Storage} from '@ionic/storage';
import {LoadingController,NavController, Loading} from "ionic-angular";
import {HomeService} from "../../services/home-service";
import {Environment} from "../../environment/environment";
import {BookDetailPage} from "../book-detail/book-detail";
import {AuthenticationService} from '../../Services/authenticate-service';


@Component({
  selector: 'page-upload',
  templateUrl: 'uploadpics.html'
})
export class UploadpicsPage {
  public images = [];
  public books: any;
  public assignments:any;
  public assign : any;
  public URL:string;
  //public loader:any;

  constructor(private auth:AuthenticationService, private storage: Storage,public loading: LoadingController,public nav: NavController,private userAssignment:HomeService) {
   this.initializeData();
  }
  
  initializeData(){
    this.auth.getCustomer().then(data=>{
      this.URL = Environment.URL+"wp/v2/assignments";
       this.userAssignment.getallAssignment(data.token).subscribe(res=>{
          this.assignments=res.json();         
      });
    })
    
     
  }

  // view book detail
  view(id:Number,courseName:string,unitName:string,lessonName:string,CID:string,LID:string){
  debugger;
    var data = {assignmentId : id,courseName:courseName,unitName:unitName,lessonName:lessonName,course_id:CID,lesson_id:LID};
    this.nav.push(BookDetailPage,data);
  }
}
