var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { Validators, FormBuilder, FormControl } from '@angular/forms';
import { NavController, AlertController, ToastController, MenuController, LoadingController } from "ionic-angular";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { HomePage } from "../home/home";
import { RegisterPage } from "../register/register";
import { AuthenticationService } from '../../Services/authenticate-service';
// import { UserService } from '../../app/user.service';
// import { WordpressService } from '../../app/wordpress.service';
var LoginPage = (function () {
    function LoginPage(navCtrl, forgotCtrl, iab, menu, loadingCtrl, formBuilder, authenticate, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.forgotCtrl = forgotCtrl;
        this.iab = iab;
        this.menu = menu;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.authenticate = authenticate;
        this.toastCtrl = toastCtrl;
        this.menu.swipeEnable(false);
        this.authenticate.getCustomer().then(function (data) {
            if (data.verified) {
                _this.navCtrl.setRoot(HomePage);
            }
        }).catch(function (e) {
        });
    }
    // go to register page
    LoginPage.prototype.register = function () {
        this.navCtrl.push(RegisterPage);
    };
    LoginPage.prototype.ionViewWillLoad = function () {
        this.login_form = this.formBuilder.group({
            username: new FormControl('', Validators.compose([
                Validators.required
            ])),
            password: new FormControl('', Validators.required)
        });
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        debugger;
        this.authenticate.signin(this.username, this.password).subscribe(function (res) {
            var data = res.json();
            if (data.token) {
                _this.username = data.user_email;
                data.verified = true;
                _this.authenticate.setUser(data);
                _this.navCtrl.setRoot(HomePage);
            }
            else {
                data.verified = true;
                _this.errorMessage = "user and password doesn't match !!!";
            }
        }, function (error) { _this.errorMessage = "user and password doesn't match !!!"; });
    };
    LoginPage.prototype.openWebsite = function () {
        var url = "https://arabic-classroom.com/register/";
        var browser = this.iab.create(url, "_blank");
        browser.show();
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Component({
        selector: 'page-login',
        templateUrl: 'login.html'
    }),
    __metadata("design:paramtypes", [NavController,
        AlertController,
        InAppBrowser,
        MenuController,
        LoadingController,
        FormBuilder,
        AuthenticationService,
        ToastController])
], LoginPage);
export { LoginPage };
//# sourceMappingURL=login.js.map