import {Component} from "@angular/core";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import {NavController, AlertController, ToastController, MenuController, LoadingController} from "ionic-angular";
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";
import {AuthenticationService} from '../../Services/authenticate-service';
// import { UserService } from '../../app/user.service';
// import { WordpressService } from '../../app/wordpress.service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  errorMessage: string;
  login_form: FormGroup;
  public username:string;
  public password:string;


  constructor(public navCtrl: NavController,  
     public forgotCtrl: AlertController,
     public iab : InAppBrowser,
     public menu: MenuController,
     public loadingCtrl: LoadingController,
     public formBuilder: FormBuilder,
     private authenticate: AuthenticationService,
     public toastCtrl: ToastController) {
    this.menu.swipeEnable(true);
    this.authenticate.getCustomer().then(data=>{
     
      
        if(data.verified){
          this.navCtrl.setRoot(HomePage);
        }
      }).catch(e=>{
  
      
    });
  }

  // go to register page
  register() {
    this.navCtrl.push(RegisterPage);
  }

  ionViewWillLoad() {
    this.login_form = this.formBuilder.group({
      username: new FormControl('', Validators.compose([
        Validators.required
      ])),
      password: new FormControl('', Validators.required)
    });
  }

  login(){   
    this.authenticate.signin(this.username , this.password).subscribe(res=>{     
      var data=res.json();    
      if(data.token){
        this.username = data.user_email;
        data.verified = true;
        this.authenticate.setUser(data);
        this.navCtrl.setRoot(HomePage);
      }
      else{
        data.verified = true;
        this.errorMessage = "The username or password is incorrect !!!"; 
      }
      
    },
    error => { this.errorMessage = "The username or password is incorrect !!!"}
  );  
     }

     openWebsite(){
      var url = "https://arabic-classroom.com/register/"
      const browser = this.iab.create(url,"_blank");
      browser.show();
  }
}

