var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from "@angular/core";
import { NavController, NavParams, ToastController } from "ionic-angular";
import { HomeService } from "../../services/home-service";
import { AuthenticationService } from "../../Services/authenticate-service";
import { BooksPage } from "../books/books";
import { PhotoViewer } from '@ionic-native/photo-viewer';
var BookDetailPage = (function () {
    function BookDetailPage(nav, photoViewer, toastCtrl, auth, navParams, userAssignment) {
        this.nav = nav;
        this.photoViewer = photoViewer;
        this.toastCtrl = toastCtrl;
        this.auth = auth;
        this.navParams = navParams;
        this.userAssignment = userAssignment;
        this.initializeData();
    }
    BookDetailPage.prototype.initializeData = function () {
        debugger;
        this.assignmentId = this.navParams.get('assignmentId');
        this.assignment_post_name = this.navParams.get('courseName');
        this.unitName = this.navParams.get('unitName');
        this.lessonName = this.navParams.get('lessonName');
        this.course_id = this.navParams.get('course_id');
        this.lesson_id = this.navParams.get('lesson_id');
        this.status = this.navParams.get('Status');
        this.file_link = this.navParams.get('file_link');
        this.feedback = this.navParams.get('feedback');
    };
    BookDetailPage.prototype.deleteAssignment = function (assignmentId, lessonId, courseId) {
        var _this = this;
        this.auth.getCustomer().then(function (data) {
            _this.userAssignment.deleteAssignment(data.token, _this.assignmentId, _this.course_id, _this.lesson_id).subscribe(function (res) {
                _this.presentToast('Assignment deleted!!!');
                _this.nav.push(BooksPage);
            });
        });
    };
    BookDetailPage.prototype.showImage = function (img) {
        this.photoViewer.show(img);
    };
    BookDetailPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    };
    return BookDetailPage;
}());
BookDetailPage = __decorate([
    Component({
        selector: 'page-book-detail',
        templateUrl: 'book-detail.html'
    }),
    __metadata("design:paramtypes", [NavController, PhotoViewer, ToastController, AuthenticationService, NavParams, HomeService])
], BookDetailPage);
export { BookDetailPage };
//# sourceMappingURL=book-detail.js.map