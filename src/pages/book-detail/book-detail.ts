import {Component} from "@angular/core";
import {NavController, NavParams, ToastController,Platform} from "ionic-angular";
import {HomeService} from "../../services/home-service";
import { AuthenticationService } from "../../Services/authenticate-service";
import {Environment} from "../../environment/environment";
import { BooksPage } from "../books/books";
import {PhotoViewer} from '@ionic-native/photo-viewer';
import { Media, MediaObject } from '@ionic-native/media';
import { File } from '@ionic-native/file';




@Component({
  selector: 'page-book-detail',
  templateUrl: 'book-detail.html'
})

export class BookDetailPage {
  public books: any;
  public assignment_name : any;
  public assignment_post_name:any;
  public assignmentId:any;
  public unitName:any;
  public lessonName:any;
  public assignment:any;
  public course_id:any;
  public lesson_id:any;
  public status:any;
  public URL:string;
  public file_link : string;
  public feedback : string; 
  playing: any[] =[];
  audio: MediaObject;
  audioList: any[] = [];
  filePath: string;
  fileName: string;
  public assignment_type : string;

  constructor(public nav: NavController,private media: Media, private file: File,public platform:Platform,public photoViewer : PhotoViewer,public toastCtrl:ToastController,public auth :AuthenticationService, public navParams: NavParams,public userAssignment : HomeService) {
      this.initializeData();
  }

  initializeData(){
    debugger;
    this.assignmentId = this.navParams.get('assignmentId');
    this.assignment_post_name = this.navParams.get('courseName');
    this.unitName = this.navParams.get('unitName');
    this.lessonName = this.navParams.get('lessonName');
    this.course_id = this.navParams.get('course_id');
    this.lesson_id = this.navParams.get('lesson_id');
    this.status = this.navParams.get('Status');
    this.file_link  = this.navParams.get('file_link');
    this.feedback = this.navParams.get('feedback');
    this.assignment_type = this.navParams.get('assignment_type');
  }
   deleteAssignment(assignmentId:string,lessonId:Number,courseId:Number){
          this.auth.getCustomer().then(data=>{
             this.userAssignment.deleteAssignment(data.token,this.assignmentId,this.course_id,this.lesson_id).subscribe(res=>{
            this.presentToast('Assignment deleted!!!');
              this.nav.push(BooksPage);
            });
          }) 
   }

   showImage(img){
    this.photoViewer.show(img);
  }

  listenAudio(file,idx){
    if (this.platform.is('ios')) {
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + file;
      this.audio = this.media.create(this.filePath);
    } else if (this.platform.is('android')) {
      // this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + file;
      this.filePath = file;
      this.audio = this.media.create(this.filePath);
    }
    this.playing[idx] = true;
    this.audio.play();
    this.audio.setVolume(1.0);
    }

    stopAudio(file,idx) {
      this.audio.stop();
      this.playing[idx] = false;
    }
    
   private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

}
