var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { ImagesProvider } from './../../providers/images/images';
import { Component } from '@angular/core';
import { ToastController, LoadingController, NavController, NavParams, ViewController } from 'ionic-angular';
import { AuthenticationService } from '../../Services/authenticate-service';
import { BookDetailPage } from '../book-detail/book-detail';
// @IonicPage() 
var UploadModalPage = (function () {
    function UploadModalPage(auth, loadingCtrl, navCtrl, navParams, toastCtrl, viewCtrl, imagesProvider) {
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.viewCtrl = viewCtrl;
        this.imagesProvider = imagesProvider;
        this.imageData = this.navParams.get('data');
        this.cdata = this.navParams.data;
    }
    UploadModalPage.prototype.saveImage = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.auth.getCustomer().then(function (user) {
            debugger;
            var token = user.token;
            _this.imagesProvider.uploadImage(_this.imageData, user.token, _this.cdata).then(function (data) {
                //this.loading.dismissAll()
                debugger;
                var sourceUrl = (JSON.parse(data.response)).source_url;
                _this.auth.postAssignment(_this.cdata, sourceUrl, token).subscribe(function (res) {
                    debugger;
                    loading.dismiss();
                    var assignDetails = res.json();
                    _this.presentToast('Assignment Uploaded successfully !!!');
                    var assignmentDetailsData = { assignmentId: assignDetails.id, assignment_post_name: assignDetails.details.course.post_name, course_id: assignDetails.details.course.ID, lesson_id: assignDetails.details.lesson.ID, lessonName: assignDetails.details.lesson.post_name, unitName: assignDetails.details.unit.post_name, file_link: assignDetails.details.file_link };
                    _this.navCtrl.push(BookDetailPage, assignmentDetailsData);
                });
                _this.presentToast('Image succesfully uploaded.');
            }, function (err) {
                _this.presentToast('Error while uploading file.');
                loading.dismiss();
            });
        });
    };
    UploadModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UploadModalPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    return UploadModalPage;
}());
UploadModalPage = __decorate([
    Component({
        selector: 'page-upload-modal',
        templateUrl: 'upload-modal.html',
    }),
    __metadata("design:paramtypes", [AuthenticationService, LoadingController, NavController, NavParams, ToastController, ViewController, ImagesProvider])
], UploadModalPage);
export { UploadModalPage };
//# sourceMappingURL=upload-modal.js.map