import { ImagesProvider } from './../../providers/images/images';
import { Component } from '@angular/core';
import { IonicPage,ToastController, LoadingController, NavController, NavParams, ViewController } from 'ionic-angular';
import {AuthenticationService} from '../../Services/authenticate-service';
import { HomeService } from '../../Services/home-service';
import { sourceUrl } from '@angular/compiler';
import { NewAssignmentPage } from '../newassignment/newassignment';
 
// @IonicPage() 
@Component({
  selector: 'page-upload-modal',
  templateUrl: 'upload-modal.html',
})
export class UploadModalPage {
  imageData: any;
  desc: string;
  cdata : any;
 
  constructor(private auth:AuthenticationService, public loadingCtrl: LoadingController, public navCtrl: NavController, private navParams: NavParams,public toastCtrl: ToastController, private viewCtrl: ViewController, private imagesProvider: ImagesProvider) {
    this.imageData = this.navParams.get('data');
    this.cdata = this.navParams.data;
  }
 
  saveImage() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();

    this.auth.getCustomer().then(user=>{
      var token = user.token;
      this.imagesProvider.uploadImage(this.imageData,user.token,this.cdata).then(data => {
        //this.loading.dismissAll()
        debugger;
        var sourceUrl = (JSON.parse(data.response)).source_url;
        this.auth.postAssignment(this.cdata,sourceUrl,token).subscribe(res=>{
          loading.dismiss();
              var assignDetails = res.json();
              this.presentToast('Assignment Uploaded successfully !!!','bottom');
              var assignmentDetailsData = {assignmentId:assignDetails.id,assignment_post_name:assignDetails.details.course.post_name,course_id:assignDetails.details.course.ID,lesson_id:assignDetails.details.lesson.ID,lessonName:assignDetails.details.lesson.post_name,unitName:assignDetails.details.unit.post_name,file_link:assignDetails.details.file_link};
              this.navCtrl.push(NewAssignmentPage);
        })
        this.presentToast('Image succesfully uploaded.','center');
      }, err => {
        this.presentToast('Error while uploading file.','bottom');
        loading.dismiss();

      });
    })
   
  }
 
  dismiss() {
    this.viewCtrl.dismiss();
  }
  private presentToast(text,pos) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: pos
    });
    toast.present();
  }
}